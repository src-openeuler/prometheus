%define    debug_package %{nil}

Name:      prometheus2
Version:   2.53.3
Release:   1
Summary:   The Prometheus 2.x monitoring system and time series database.
License:   Apache-2.0
URL:       https://prometheus.io
Source0:   https://github.com/prometheus/prometheus/archive/v%{version}/prometheus-%{version}.tar.gz
# tar -xvf Source0
# run 'go mod vendor' in it
# tar -czvf prometheus-vendor.tar.gz vendor
Source1:   prometheus-vendor.tar.gz
Source3:   prometheus.service
Source4:   prometheus.default
Source5:   prometheus.yml
Source6:   prometheus.logrotate

Patch0:    Change-client_golang-dependency-version-to-v1.20.0.patch

BuildRequires: systemd
BuildRequires: golang
BuildRequires: npm
BuildRequires: promu

%{?systemd_requires}
Requires(pre): shadow-utils

Conflicts: prometheus

%description
Prometheus is a systems and service monitoring system. It collects metrics from
configured targets at given intervals, evaluates rule expressions, displays the
results, and can trigger alerts if some condition is observed to be true.

%prep
%setup -n prometheus-%{version}
tar -xzvf %{SOURCE1}
%patch 0 -p1
# Compile using the promu installed on the system, not the downloaded one
sed -i 's|PROMU[[:space:]]*:=.*$(FIRST_GOPATH)/bin/promu|PROMU        := /bin/promu|' Makefile.common

%build
export GOFLAGS="-mod=vendor -buildmode=pie"
export BUILDTAGS="netgo builtinassets"
make build

%install
mkdir -vp %{buildroot}%{_sharedstatedir}/prometheus
install -D -m 755 prometheus %{buildroot}%{_bindir}/prometheus
install -D -m 755 promtool %{buildroot}%{_bindir}/promtool
install -D -m 644 %{SOURCE3} %{buildroot}%{_unitdir}/prometheus.service
install -D -m 644 %{SOURCE4} %{buildroot}%{_sysconfdir}/default/prometheus
install -D -m 644 %{SOURCE5} %{buildroot}%{_sysconfdir}/prometheus/prometheus.yml
install -D -m 644 %{SOURCE6} %{buildroot}%{_sysconfdir}/logrotate.d/prometheus

for dir in console_libraries consoles; do
  for file in ${dir}/*; do
    install -D -m 644 ${file} %{buildroot}%{_sysconfdir}/prometheus/${file}
  done
done

%pre
getent group prometheus >/dev/null || groupadd -r prometheus
getent passwd prometheus >/dev/null || \
  useradd -r -g prometheus -d %{_sharedstatedir}/prometheus -s /sbin/nologin \
          -c "Prometheus services" prometheus
exit 0

%post
%systemd_post prometheus.service

%preun
%systemd_preun prometheus.service

%postun
%systemd_postun prometheus.service

%files
%license LICENSE NOTICE
%{_bindir}/*
%dir %{_sysconfdir}/prometheus/
%dir %{_sysconfdir}/prometheus/consoles
%dir %{_sysconfdir}/prometheus/console_libraries
%{_sysconfdir}/prometheus/consoles/*
%{_sysconfdir}/prometheus/console_libraries/*
%config(noreplace) %{_sysconfdir}/logrotate.d/prometheus
%config(noreplace) %{_sysconfdir}/prometheus/prometheus.yml
%{_unitdir}/prometheus.service
%config(noreplace) %{_sysconfdir}/default/prometheus
%dir %attr(755, prometheus, prometheus)%{_sharedstatedir}/prometheus

%changelog
* Wed Dec 04 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 2.53.3-1
- Update package to version 2.53.3
  * Fix metric name include histogram error
  * TSDB: Let queries with heavy regex matches be cancelled and not use up the CPU
  * Scraping: allow multiple samples on same series, with explicit timestamps

* Tue Jun 25 2024 jiangxinyu <jiangxinyu@kylinos.cn> - 2.52.0-1
- Update package to version 2.52.0
- Fix possible duplicated label name and values in a metric result for specific queries
- Fix counting of histogram samples when creating WAL checkpoint stats

* Sun Dec 31 2023 menmazqj <qijia.oerv@isrc.iscas.ac.cn> - 2.46.0-2
- add riscv64 support for prometheus

* Fri Sep 08 2023 jiangxinyu <jiangxinyu@kylinos.cn> - 2.46.0-1
- Update package to version 2.46.0

* Tue Jun  6 2023 Wenlong Zhang <zhangwenlong@loongson.cn> - 2.20.0-4
- add loong64 support for prometheus

* Wed Apr 20 2022 zhuang.li <zhuang.li@turbolinux.com.cn> - 2.20.0-3
- Modify the schema judgment, resulting in compilation failure

* Tue Dec 14 2021 konglidong <konglidong@uniontech.com> - 2.20.0-2
- modify format and delete %dist

* Tue Aug 11 2020 houjian <houjian@kylinos.cn> - 2.20.0-1
- Init project prometheus
